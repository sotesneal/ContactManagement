//create contact model class
 class Contact {
    constructor(id, firstName, lastName, billingAddress, physicalAddress) {
        //generate random id
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.billingAddress = billingAddress;
        this.physicalAddress = physicalAddress;
    }
}
export default Contact;
