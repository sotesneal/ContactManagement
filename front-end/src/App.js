import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import LoginForm from './components/pages/loginForm';
import RegistrationForm from './components/pages/registrationForm';
import Dashboard from './components/pages/dashboard';
import PrivateRoute from './components/pages/privateRoute';
import PublicRoute from './components/pages/publicRoute';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import 'tailwindcss/tailwind.css'

function App() {
  return (
    <>
    <Router>
      <Routes>
        <Route path="/" element={<PublicRoute><LoginForm /></PublicRoute>} />
        <Route path="/login" element={<PublicRoute><LoginForm /></PublicRoute>} /> {/* Added this line */}
        <Route path="/register" element={<PublicRoute><RegistrationForm /></PublicRoute>} />
        <Route path="/dashboard" element={<PrivateRoute><Dashboard /></PrivateRoute>} />
      </Routes>
    </Router>
    <ToastContainer />
    </>
  );
}

export default App;
