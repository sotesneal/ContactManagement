import React from 'react'
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';
import ModalForm from '../../components/modals/Modal';
import ContactService from '../../components/services/contact';

function DataTable(props) {
  const { contacts } = props;
  const columns = [
    { field: 'id', headerName: 'ID', flex: 2 },
    { field: 'firstName', headerName: 'First name', flex: 2 },
    { field: 'lastName', headerName: 'Last name', flex: 2 },
    { field: 'billingAddress', headerName: 'Billing address', flex: 2 },
    { field: 'physicalAddress', headerName: 'Physical address', flex: 3 },
    {
      field: 'actions',
      headerName: 'Actions',
      flex: 2,
      renderCell: (params) => (
        <strong>
          {params.row.id ? (
            <>
              <ModalForm buttonLabel="Edit" item={params.row} updateState={props.updateState} />
              {' '}
              <Button variant="contained" color="secondary" onClick={() => deleteItem(params.row.id)}>Delete</Button>
            </>
          ) : null}
        </strong>
      ),
    },
  ];

  //delete contact
  function deleteItem(id){
    let confirmDelete = window.confirm('Delete contact forever?')
    if(confirmDelete){
      ContactService.deleteContact(id)
      .then(response => {
        props.deleteItemFromState(id)
      })
      .catch(err => console.log(err))
    }
  }

  return (
    //make it responsive
    <div className="w-full h-full">
      <DataGrid autoHeight rows={contacts} columns={columns} pageSize={5} className="overflow-auto" />
    </div>
  );
}

export default DataTable;
