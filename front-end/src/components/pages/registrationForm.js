import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function RegistrationForm() {
    const [state, setState] = useState({
        firstname: "",
        lastname: "",
        email: "",
        username: "",
        password: "",
        errors: {},
        success: "",  // Add success field to state
    });

    const navigate = useNavigate();

    const handleChange = (e) => {
        const { id, value } = e.target;
        setState(prevState => ({
            ...prevState,
            [id]: value,
            errors: {
                ...prevState.errors,
                [id]: ''
            }
        }));
    };

    const validateForm = () => {
        let formErrors = {};
    
        if (!state.firstname) {
            formErrors.firstname = "First name is required.";
        }
    
        if (!state.lastname) {
            formErrors.lastname = "Last name is required.";
        }
    
        if (!state.email) {
            formErrors.email = "Email is required.";
        } else {
            // Check email validity using regex
            // eslint-disable-next-line no-control-regex
            const pattern = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/);
            if (!pattern.test(state.email)) {
                formErrors.email = "Please enter a valid email address.";
            }
        }
    
        if (!state.username) {
            formErrors.username = "Username is required.";
        } else if (state.username.length < 4 || state.username.length > 12) {
            formErrors.username = "Username should be between 4 and 12 characters.";
        }
    
        if (!state.password) {
            formErrors.password = "Password is required.";
        } else {
            // Check password complexity using regex
            const pattern = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/);
            if (!pattern.test(state.password)) {
                formErrors.password = "Password should have at least 8 characters, one uppercase letter, one lowercase letter, one number and one special character.";
            }
        }
    
        setState(prevState => ({
            ...prevState,
            errors: formErrors
        }));
    
        // If the formErrors object is empty, the form is valid.
        return Object.keys(formErrors).length === 0;
    };
    

    const handleSubmitClick = (e) => {
        e.preventDefault();
    
        if (!validateForm()) {
            return;
        }
    
        const payload = {
            firstName: state.firstname,
            lastName: state.lastname,
            email: state.email,
            username: state.username,
            password: state.password
        };
    
        axios.post('http://localhost:8080/api/authentication/register', payload)
            .then((res) => {
                toast.success('Registration successful!', {
                    position: toast.POSITION.TOP_CENTER,
                    onClose: () => navigate('/')
                });
            })
            .catch((error) => {
                if (error.response) {
                    let errMsg = 'Registration failed!'; 
                    if (error.response.data.error === 'Username already exists') {
                        errMsg = 'Username already exists. Please try another one.';
                    } else if (error.response.data.error === 'Email already exists') {
                        errMsg = 'Email already exists. Please try another one.';
                    }
    
                    setState(prevState => ({
                        ...prevState,
                        errors: {
                            ...prevState.errors,
                            username: error.response.data.error,
                        }
                    }));
    
                    toast.error(errMsg, {
                        position: toast.POSITION.TOP_CENTER
                    });
                }
            });
    };
    


    

    return (
        <div className="d-flex justify-content-center align-items-center" style={{ height: '100vh' }}>
            <div className="card p-4" style={{ width: '450px' }}>
                <h2 className="text-center mb-4">Registration</h2>
                {state.success && <div className="alert alert-success">{state.success}</div>}
                <form>
                    <div className="form-group">
                        <label htmlFor="firstname">First Name</label>
                        <input
                            type="text"
                            className={`form-control ${state.errors.firstname ? 'is-invalid' : ''}`}
                            id="firstname"
                            placeholder="Enter first name"
                            value={state.firstname}
                            onChange={handleChange}
                        />
                        {state.errors.firstname && <small className="text-danger">{state.errors.firstname}</small>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastname">Last Name</label>
                        <input
                            type="text"
                            className={`form-control ${state.errors.lastname ? 'is-invalid' : ''}`}
                            id="lastname"
                            placeholder="Enter last name"
                            value={state.lastname}
                            onChange={handleChange}
                        />
                        {state.errors.lastname && <small className="text-danger">{state.errors.lastname}</small>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email address</label>
                        <input
                            type="email"
                            className={`form-control ${state.errors.email ? 'is-invalid' : ''}`}
                            id="email"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                            value={state.email}
                            onChange={handleChange}
                        />
                        {state.errors.email && <small className="text-danger">{state.errors.email}</small>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input
                            type="text"
                            className={`form-control ${state.errors.username ? 'is-invalid' : ''}`}
                            id="username"
                            placeholder="Enter username"
                            value={state.username}
                            onChange={handleChange}
                        />
                        {state.errors.username && <small className="text-danger">{state.errors.username}</small>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className={`form-control ${state.errors.password ? 'is-invalid' : ''}`}
                            id="password"
                            placeholder="Password"
                            value={state.password}
                            onChange={handleChange}
                        />
                        {state.errors.password && <small className="text-danger">{state.errors.password}</small>}
                    </div>
                    <button
                        type="submit"
                        className="btn btn-primary w-100 mt-3"
                        onClick={handleSubmitClick}
                    >
                        Register
                    </button>
                    <div className="text-center mt-3">
                        Already have an account? <Link to="/">Login</Link>
                    </div>
                </form>
            </div>
        </div>
    );
    
}

export default RegistrationForm;
