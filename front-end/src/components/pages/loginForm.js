import React, { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';
import { useNavigate, Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import Loading from 'react-loading';
import { toast } from 'react-toastify';



function LoginForm() {
    const [state, setState] = useState({
        username: "",
        password: "",
        errorMessage: "",
        successMessage: "",
        errors: {},
    });
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        if (showModal) {
            setTimeout(() => {
                setShowModal(false);
                navigate('/dashboard');
            }, 2000);
        }
    }, [showModal, navigate]);
    

    const handleChange = (e) => {
        const { id, value } = e.target;
        setState(prevState => ({
            ...prevState,
            [id]: value,
            errorMessage: "",
            errors: {
                ...prevState.errors,
                [id]: ''
            }
        }));
    };
    

    const validateForm = () => {
        let formErrors = {};

        if (!state.username) {
            formErrors.username = "Username is required.";
        }

        if (!state.password) {
            formErrors.password = "Password is required.";
        }

        setState(prevState => ({
            ...prevState,
            errors: formErrors
        }));

        // If the formErrors object is empty, the form is valid.
        return Object.keys(formErrors).length === 0;
    };

    const handleSubmitClick = (e) => {
        e.preventDefault();
    
        if (!validateForm()) {
            return;
        }
        
        setIsLoading(true);
        const payload = {
            Username: state.username,
            Password: state.password
        }
    
        setState(prevState => ({
            ...prevState,
            successMessage: "Logging in...",
            errors: {}
        }));
    
        const loginRequest = axios.post('http://localhost:8080/api/authentication/login', payload);
    
        loginRequest
        .then(function (response) {
            if (response.status === 200) {
                console.log('Login successful');
                localStorage.setItem('token', response.data.token);
                setIsLoading(false);  // stop the loading icon
                toast.success("Login successful! Redirecting...", {
                    position: toast.POSITION.TOP_CENTER,
                    autoClose: 1000,
                    onClose: () => {
                        setTimeout(() => {
                            navigate('/dashboard');
                        }, 2000);
                    }
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            setIsLoading(false);  // stop the loading icon
    
            if (error.response) {
                setState(prevState => ({
                    ...prevState,
                    errorMessage: error.response.data.error
                }));
            }
        });
    }
    

    return (
        <div className="d-flex justify-content-center align-items-center" style={{ height: '100vh' }}>
                <div className="card p-4" style={{ width: '400px', height: '400px' }}>
                <div className="card-body">
                    <h3 className="card-title text-center">Login</h3>
                    <form>
                        <div className="form-group text-left">
                            <label htmlFor="username">Username</label>
                            <input type="text"
                                className={`form-control my-2 ${state.errors.username ? 'is-invalid' : ''}`}
                                id="username"
                                placeholder="Enter username"
                                value={state.username}
                                onChange={handleChange}
                            />
                            {state.errors.username && <small className="text-danger">{state.errors.username}</small>}
                        </div>
                        <div className="form-group text-left">
                            <label htmlFor="password">Password</label>
                            <input type="password"
                                className={`form-control my-2 ${state.errors.password ? 'is-invalid' : ''}`}
                                id="password"
                                placeholder="Password"
                                value={state.password}
                                onChange={handleChange}
                            />
                            {state.errors.password && <small className="text-danger">{state.errors.password}</small>}
                                </div>
                        {state.errorMessage && <div className="alert alert-danger mt-2" role="alert">
                            {state.errorMessage}
                        </div>}
                        <button
                            type="submit"
                            className="btn btn-primary w-100 mt-3"
                            onClick={handleSubmitClick}
                        >
                            Login
                        </button>
                    </form>
                    <div className="mt-2">
                        Not yet registered? <Link to="/register">Register</Link>
                    </div>
                    <Modal 
                        show={showModal} 
                        onHide={() => setShowModal(false)} 
                        size="md"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        >
                        <Modal.Body>
                            <div style={{ 
                                display: 'flex', 
                                flexDirection: 'column', 
                                alignItems: 'center', 
                                justifyContent: 'center',
                                padding: '20px',
                                color: '#4F8A10',
                                backgroundColor: '#DFF2BF',
                                borderRadius: '15px',
                                boxShadow: '0 0 10px rgba(0,0,0,0.1)',
                                fontFamily: 'Arial, sans-serif',
                                fontSize: '18px',
                                fontWeight: 'bold',
                            }}>
                                <FontAwesomeIcon icon={faCheckCircle} style={{ fontSize: '40px', marginBottom: '15px' }} />
                                Login Successful! Redirecting...
                            </div>
                        </Modal.Body>
                    </Modal>

                </div>
            </div>
            {isLoading && 
                <div style={{
                    position: "fixed",
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                }}>
                    <Loading type={'spin'} color={'#123abc'} height={'10%'} width={'10%'} />
                </div>
            }
        </div>
    )
}

export default LoginForm;
