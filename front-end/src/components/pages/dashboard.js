import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col, Button, Alert } from 'reactstrap';
import ModalForm from '../modals/Modal';
import DataTable from '../tables/DataTable';
import services from '../services/contact';
import { ToastContainer, toast } from 'react-toastify';

const Dashboard = () => {
  const [originalContacts, setOriginalContacts] = useState([]); 
  const [contacts, setContacts] = useState([]);
  const [deleteSuccess, setDeleteSuccess] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    getItems();
  }, []);

  const getItems = async () => {
    const response = await services.getAllContacts();
    setOriginalContacts(response.data); 
    setContacts(response.data);
  }

  const addItemToState = (contacts) => {
    setContacts(prevState => [...prevState, contacts]);
    setOriginalContacts(prevState => [...prevState, contacts]);
  }

  const searchItem = (e) => {
    let searchWords = e.target.value.toLowerCase().split(' ');
  
    if(searchWords.length === 1 && searchWords[0] === ''){
      setContacts(originalContacts);
    } else {
      let filteredContacts = originalContacts.filter((item) => {
        let firstName = item.firstName ? item.firstName.toLowerCase() : "";
        let lastName = item.lastName ? item.lastName.toLowerCase() : "";
        let fullName = `${firstName} ${lastName}`;
        let id = item.id ? item.id.toString().toLowerCase() : "";
        let billingAddress = item.billingAddress ? item.billingAddress.toLowerCase() : "";
        let physicalAddress = item.physicalAddress ? item.physicalAddress.toLowerCase() : "";
  
        return searchWords.every(word => 
          fullName.includes(word) || 
          firstName.includes(word) || 
          lastName.includes(word) || 
          id.includes(word) || 
          billingAddress.includes(word) || 
          physicalAddress.includes(word)
        );
      });
  
      if(filteredContacts.length === 0){
        setContacts([{id: 0, firstName: 'No Record Found'}]);
      } else {
        setContacts(filteredContacts);
      }
    }
  }
  

  const updateState = (updatedContact) => {
    const itemIndex = contacts.findIndex(contact => contact.id === updatedContact.id)
    if(itemIndex !== -1){
        const newArray = [
            ...contacts.slice(0, itemIndex),
            updatedContact,
            ...contacts.slice(itemIndex + 1)
        ]
        setContacts(newArray);
        setOriginalContacts(newArray); 
    }
  }

  const deleteItemFromState = (id) => {
    const updatedItems = contacts.filter(item => item.id !== id);
    setContacts(updatedItems);
    setOriginalContacts(updatedItems);

    // Display a toast notification
    toast.success('Contact successfully deleted!', {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 3000
    });
  }
  const logout = () => {
    localStorage.removeItem('token');
    navigate('/');
  }

  return (
    <Container className="Dashboard">
      <ToastContainer />
      <Row>
        <Col className="d-flex justify-content-end">
          <Button style={{margin: "20px 0 0 0"}} color="danger" onClick={logout}>Logout</Button>
        </Col>
      </Row>
      {deleteSuccess && // If deleteSuccess is true, render the Alert
        <Row>
          <Col>
            <Alert color="success" className="mt-3">
              Contact successfully deleted!
            </Alert>
          </Col>
        </Row>
      }
      <Row>
        <Col>
          <h1 style={{margin: "40px 0", textAlign:"center"}}>User Contact Management Dashboard</h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <input style={{marginBottom: "15px"}} type="text" className='form-control w-25 mr-sm-2' placeholder="Search" onChange={(e) => searchItem(e)} />
        </Col>
      </Row>
      <Row>
        <Col>
          <DataTable contacts={contacts} updateState={updateState} deleteItemFromState={deleteItemFromState} />
        </Col>
      </Row>
      <Row>
        <Col style={{marginTop: "15px"}}>
          <ModalForm buttonLabel="Add Contact" addItemToState={addItemToState} />
        </Col>
      </Row>
    </Container>
  );
}

export default Dashboard;
