import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';

const PublicRoute = ({ children }) => {
  const location = useLocation();
  const token = localStorage.getItem('token');

  // If there's a token, the user is authenticated and should be redirected to the dashboard
  return token ? <Navigate to="/dashboard" /> : children;
};

export default PublicRoute;
