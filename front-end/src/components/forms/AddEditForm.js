/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import ContactService from '../../components/services/contact';
import { toast } from 'react-toastify';


class AddEditForm extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    billingAddress: '',
    physicalAddress: '',
    successMessage: '',
    errorMessage: '' 
  }
  
  componentDidMount(){
    // if item exists, populate the state with proper data
    if(this.props.item){
      const { id, firstName, lastName, billingAddress, physicalAddress} = this.props.item
      this.setState({ id, firstName, lastName, billingAddress, physicalAddress})
    }
  }
  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  
    const value = e.target.value.trim();
    
    // Regular expression to check if the string only contains letters and spaces
    const nameRegex = /^[A-Za-z\s]+$/;
  
    //add validation first name
    if(e.target.name === 'firstName'){
      if(value.length < 3 || !nameRegex.test(value)){
        this.setState({firstNameError: 'First name must be at least 3 characters long and only contain letters'})
      }else{
        this.setState({firstNameError: ''})
      }
    }
    //add validation last name
    if(e.target.name === 'lastName'){
      if(value.length < 3 || !nameRegex.test(value)){
        this.setState({lastNameError: 'Last name must be at least 3 characters long and only contain letters'})
      }else{
        this.setState({lastNameError: ''})
      }
    }
    //add validation billing address
    if(e.target.name === 'billingAddress'){
      if(value.length < 10){
        this.setState({billingAddressError: 'Billing address must be at least 10 characters long'})
      }else{
        this.setState({billingAddressError: ''})
      }
    }
    //add validation physical address
    if(e.target.name === 'physicalAddress'){
      if(value.length < 10){
        this.setState({physicalAddressError: 'Physical address must be at least 10 characters long'})
      }else{
        this.setState({physicalAddressError: ''})
      }
    }
  }
  
 //submit form to create new contact use service
 submitFormAdd = e => {
  e.preventDefault()
  ContactService.createContact(this.state)
  .then(response => {
    this.props.addItemToState(response.data)
    toast.success('Contact successfully added.');
    // Close the form after a delay
    setTimeout(() => this.props.toggle(), 1000);
    this.setState({errorMessage: ''}); // Clear the error message on success
  })
  .catch(err => {
    console.log(err);
    if(err.response && err.response.data){
      this.setState({errorMessage: err.response.data});
    }
  })
}
  

submitFormEdit = e => {
  e.preventDefault();
  const { id, ...contactData } = this.state;
  ContactService.updateContact(id, contactData)
    .then(res => {
      ContactService.getAllContacts()
        .then(response => {
          const duplicateContact = response.data.find(contact => contact.id !== id && contact.firstName === contactData.firstName && contact.lastName === contactData.lastName);
          if (duplicateContact) {
            throw new Error("A different contact with the same first name and last name already exists.");
          } else {
            this.props.updateState(this.state);
            toast.success('Contact successfully updated.'); // Use toast.success here
            setTimeout(() => this.props.toggle(), 1000);
            this.setState({errorMessage: ''});
          }
        })
    })
    .catch(err => {
      if (err.response) {
        console.log('Server error:', err.response.data);
      } else {
        console.log('Axios error:', err.message);
      }
      if(err.response && err.response.data){
        this.setState({errorMessage: err.response.data});
      }
    });
}


  onSubmit = (e) => {
    e.preventDefault();
    if(this.state.firstNameError || this.state.lastNameError || this.state.billingAddressError || this.state.physicalAddressError){
      this.setState({errorMessage: 'Please fill out all fields correctly'})
    }else{
      if(this.state.id) {
        this.submitFormEdit(e);
      } else {
        this.submitFormAdd(e);
      }
    }
  }
  
    

  render() {
    return (
     
        <Form onSubmit={this.onSubmit} className="mt-2">
          <FormGroup className="mb-2">
            <Label for="firstName" className="mb-2">First Name</Label>
            <Input type="text" required name="firstName" id="firstName" onChange={this.onChange} value={this.state.firstName === null ? '' : this.state.firstName} className="form-control" />
            {this.state.firstNameError ? <div className="text-danger">{this.state.firstNameError}</div> : null}
          </FormGroup>
          <FormGroup className="mb-2">
            <Label for="lastName" className="mb-2">Last Name</Label>
            <Input type="text" required name="lastName" id="lastName" onChange={this.onChange} value={this.state.lastName === null ? '' : this.state.lastName} className="form-control" />
            {this.state.lastNameError ? <div className="text-danger">{this.state.lastNameError}</div> : null}
          </FormGroup>
          <FormGroup className="mb-2">
            <Label for="billingAddress" className="mb-2">Billing Address</Label>
            <Input type="text" required name="billingAddress" id="billingAddress" onChange={this.onChange} value={this.state.billingAddress === null ? '' : this.state.billingAddress} className="form-control" />
            {this.state.billingAddressError ? <div className="text-danger">{this.state.billingAddressError}</div> : null}
          </FormGroup>
          <FormGroup className="mb-2">
            <Label for="physicalAddress" className="mb-2">Physical Address</Label>
            <Input type="text" required name="physicalAddress" id="physicalAddress" onChange={this.onChange} value={this.state.physicalAddress === null ? '' : this.state.physicalAddress} className="form-control" />
            {this.state.physicalAddressError ? <div className="text-danger">{this.state.physicalAddressError}</div> : null}
          </FormGroup>
        
          {this.state.errorMessage && 
          <div className="alert alert-danger" role="alert">
              {this.state.errorMessage}
          </div>
          }
          <Button color='success' className="btn btn-primary btn-block">Submit</Button>
          {this.state.successMessage ? 
          <div className="alert alert-success mt-3" role="alert">
              <i className="fas fa-check-circle"></i> {this.state.successMessage}
          </div>
          : null
          }
      </Form>
    );
  }
}

export default AddEditForm
