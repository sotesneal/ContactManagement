import { Component } from "react";
import {environment} from '../../environment';
import axios from "axios";



//services class for fetching data from the API
class ContactService extends Component {
  //get all contacts
  getAllContacts = () => {
    return axios.get(`${environment.API_URL}/contacts`);
  };

  //get contact by id
  getContactById = (id) => {
    return axios.get(`${environment.API_URL}/contacts/${id}`);
  };

//create contact
createContact = (contact) => {
  return axios.post(`${environment.API_URL}/contacts`, contact, {
    headers: { 'Content-Type': 'application/json' }
  });
};


//update contact
updateContact = (id, contact) => {
  console.log('Updating contact with data:', { id, ...contact }); // Log data before making request
  return axios.put(`${environment.API_URL}/contacts/${id}`, contact);
};


  //delete contact
  deleteContact = (id) => {
    return axios.delete(`${environment.API_URL}/contacts/${id}`);
  };
}

export default new ContactService();