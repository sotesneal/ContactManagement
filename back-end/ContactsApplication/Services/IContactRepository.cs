using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApplication.Model;

namespace ContactsApplication.Services
{
    public interface IContactRepository
    {
        Task<IEnumerable<Contact>> GetContactsAsync();
        Task<Contact?> GetContactAsync(int id);
        Task<Contact?> AddContactAsync(Contact contact);
        Task UpdateContactAsync(Contact contact);
        Task SaveChangesAsync();
        Task<bool> ContactExistsAsync(string firstName, string lastName, int? id = null);
        Task DeleteContactAsync(int id);
    }
}
