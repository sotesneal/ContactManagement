using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApplication.Model;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace ContactsApplication.Services
{
   
    public class ContactRepository : IContactRepository
    {
        private readonly ContactContext _context;
        private readonly IUserService _userService;
        private readonly ILogger<ContactRepository> _logger;
        
        public ContactRepository(ContactContext context, IUserService userService, ILogger<ContactRepository> logger)
        {
            _context = context;
            _userService = userService;
            _logger = logger;
        }

        public async Task<IEnumerable<Contact>> GetContactsAsync()
        {
            return await _context.Contacts.ToListAsync();
        }

        public async Task<Contact?> GetContactAsync(int id)
        {
            var contact = await _context.Contacts.FindAsync(id);
            return contact;
        }

        public async Task<Contact?> AddContactAsync(Contact contact)
        {
            contact.CreatedDate = DateTime.UtcNow;

            var username = _userService.GetCurrentUserName();
            if (!string.IsNullOrEmpty(username))
            {
                // If user is authenticated, use their username.
                contact.CreatedBy = username;
                contact.ModifiedBy = username; 
            }
            else
            {
                // If user is not authenticated, use a system user ID.
                contact.CreatedBy = "system";
                contact.ModifiedBy = "system";
            }
            var existingContact = await _context.Contacts
                .Where(c => c.FirstName.ToUpper() == contact.FirstName.ToUpper() && c.LastName.ToUpper() == contact.LastName.ToUpper())
                .FirstOrDefaultAsync();

            if(existingContact != null)
            {
                throw new ArgumentException("A contact with the same first name and last name already exists.");
            }

            contact.FirstName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.FirstName.ToLower());
            contact.LastName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.LastName.ToLower());
            contact.BillingAddress = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.BillingAddress.ToLower());
            contact.PhysicalAddress = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.PhysicalAddress.ToLower());

            _context.Contacts.Add(contact);
            await _context.SaveChangesAsync();
            return contact;
        }

        public async Task UpdateContactAsync(Contact contact)
        {
            // Implementation of UpdateContactAsync goes here...
        }

        public async Task DeleteContactAsync(int id)
        {
            var existingContact = await _context.Contacts.FindAsync(id);

            if(existingContact == null)
            {
                throw new Exception("Contact does not exist.");
            }

            _context.Contacts.Remove(existingContact);
            await _context.SaveChangesAsync();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ContactExistsAsync(string firstName, string lastName, int? id = null)
        {
            if (id == null)
            {
                return await _context.Contacts.AnyAsync(c => c.FirstName == firstName && c.LastName == lastName);
            }

            return await _context.Contacts.AnyAsync(c => c.FirstName == firstName && c.LastName == lastName && c.Id != id.Value);
        }
    }

}
