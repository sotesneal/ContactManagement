using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApplication.Model.Authentication;

namespace ContactsApplication.Services
{
    public interface IUserService
    {
        Task<User> AuthenticateAsync(string username, string password);
        Task<string> GenerateJWTAsync(User user);
        Task<User> RegisterAsync(UserRegistrationModel model);
        int? GetCurrentUserId();
        string? GetCurrentUserName();
    }
}
