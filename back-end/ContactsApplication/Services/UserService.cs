using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApplication.Model.Authentication;
using System.Text;
using ContactsApplication.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore;

namespace ContactsApplication.Services
{
    public class UserService : IUserService
    {
        private readonly ContactContext _context;
        private readonly JwtSettings _jwtSettings;
        private IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<UserService> _logger; 

        public UserService(ContactContext context, IOptions<JwtSettings> jwtsettings, IHttpContextAccessor httpContextAccessor, ILogger<UserService> logger)
        {
            _context = context;
            _jwtSettings = jwtsettings.Value;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }

        public async Task<User> AuthenticateAsync(string username, string password)
        {
            var user = await _context.Users.SingleOrDefaultAsync(x => x.Username == username);

            if (user == null)
            {
                throw new UnauthorizedAccessException("Username or password is incorrect");
            }

            string hashedInputPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: new byte[0], 
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            if (user.Password == hashedInputPassword)
            {
                return user;
            }
            else
            {
                throw new UnauthorizedAccessException("Username or password is incorrect");
            }
        }

        public async Task<string> GenerateJWTAsync(User user)
        {
            Console.WriteLine($"Secret Key: {_jwtSettings.SecretKey}");
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.SecretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim("id", user.Id.ToString())
            };

            var jwt = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                claims: claims,
                expires: DateTime.Now.AddHours(3),
                signingCredentials: credentials);

            return await Task.FromResult(new JwtSecurityTokenHandler().WriteToken(jwt));
        }

        public async Task<User> RegisterAsync(UserRegistrationModel model)
        {
            var existingUser = await _context.Users.SingleOrDefaultAsync(x => x.Username == model.Username);
            
            if (existingUser != null)
            {
                throw new Exception("Username is already taken.");
            }
            
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: model.Password,
                salt: new byte[0],
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            var newUser = new User 
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Username = model.Username,
                Password = hashed
            };
            _context.Users.Add(newUser);
            await _context.SaveChangesAsync();

            return newUser;
        }
       public int? GetCurrentUserId()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext?.User is null)
            {
                // Return null if the user is not authenticated.
                return null;
            }

            var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "id");
            if (userIdClaim == null)
            {
                // Return null if the user ID claim is not found.
                return null;
            }

            if (int.TryParse(userIdClaim.Value, out int userId))
            {
                return userId;
            }

            // Log a warning or handle the situation when the user ID claim value is not a valid integer.
            _logger.LogWarning($"Invalid user ID claim value: {userIdClaim.Value}");

            return null;
        }
        public string? GetCurrentUserName()
        {
            //null check
            if (_httpContextAccessor.HttpContext?.User is null)
            {
                return null;
            }
            var usernameClaim = _httpContextAccessor.HttpContext.User.Claims
                .FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
            return usernameClaim?.Value;
        }


    }
}
