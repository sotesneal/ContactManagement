﻿using System;
using System.IO;
using System.Text.Json;
using System.Linq;
using ContactsApplication.Model;

namespace ContactsApplication.Services
{
    public interface IContactsService
    {
        IEnumerable<Contact> GetContacts();
        Contact? GetContact(int id);
        void AddContact(Contact contact);
        void UpdateContact(Contact contact);
        void DeleteContact(Contact contact);
    }

    public class JsonFileContactsService : IContactsService
    {
        private readonly string _fileName = "contacts.json";
        private Contact[]? _contacts;

        public JsonFileContactsService()
        {
            LoadContacts();
        }

        private void LoadContacts()
        {
            if (!File.Exists(_fileName))
            {
                _contacts = Array.Empty<Contact>();
                return;
            }

            string jsonString = File.ReadAllText(_fileName);
            _contacts = JsonSerializer.Deserialize<Contact[]>(jsonString)!;
        }

        private void WriteToFile()
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            File.WriteAllText(_fileName, JsonSerializer.Serialize(_contacts, options));
        }

        public void AddContact(Contact contact)
        {
            if (contact == null)
            {
                throw new ArgumentNullException(nameof(contact));
            }
            if (_contacts == null)
            {
                throw new InvalidOperationException("Contacts have not been loaded");
            }
            if (_contacts.Any(c => c.Id == contact.Id))
            {
                throw new ArgumentException("Contact with this id already exists");
            }

            Array.Resize(ref _contacts, _contacts.Length + 1);
            _contacts[_contacts.Length - 1] = contact;
            WriteToFile();
        }

        public void DeleteContact(Contact contact)
        {
            if (contact == null)
            {
                throw new ArgumentNullException(nameof(contact));
            }

            if (_contacts == null)
            {
                throw new InvalidOperationException("Contacts have not been loaded");
            }
            int index = Array.FindIndex(_contacts, c => c.Id == contact.Id);
            if (index == -1)
            {
                throw new ArgumentException("Contact with this id does not exist");
            }

            _contacts = _contacts.Where((val, idx) => idx != index).ToArray();
            WriteToFile();
        }

        public Contact GetContact(int id)
        {
            return _contacts?.FirstOrDefault(c => c.Id == id)!;
        }

        public IEnumerable<Contact> GetContacts()
        {
            if (_contacts == null)
            {
                throw new InvalidOperationException("Contacts have not been loaded");
            }
            return _contacts.ToList();
        }

        public void UpdateContact(Contact contact)
        {
            if (contact == null)
            {
                throw new ArgumentNullException(nameof(contact));
            }
            if (_contacts == null)
            {
                throw new InvalidOperationException("Contacts have not been loaded");
            }
            int index = Array.FindIndex(_contacts, c => c.Id == contact.Id);
            if (index == -1)
            {
                throw new ArgumentException("Contact with id does not exist");
            }

            _contacts[index].FirstName = contact.FirstName;
            _contacts[index].LastName = contact.LastName;
            _contacts[index].BillingAddress = contact.BillingAddress;
            _contacts[index].PhysicalAddress = contact.PhysicalAddress;
            WriteToFile();
        }
    }
}
