using Microsoft.EntityFrameworkCore;
using ContactsApplication.Services;
using ContactsApplication.Model;
using ContactsApplication.Model.Authentication;

public class ContactContext : DbContext
{
    public DbSet<Contact> Contacts { get; set; } = null!;

    public DbSet<User> Users { get; set; } = null!;

    public ContactContext(DbContextOptions<ContactContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //modelBuilder.Entity<Contact>().ToTable("Contacts");
        modelBuilder.Entity<Contact>()
            .HasIndex(c => new { c.FirstName, c.LastName })
            .IsUnique();
    }
}