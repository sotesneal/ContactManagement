using System.Web.Http;
using System.Web.Http.Cors;
using ContactsApplication.Configuration;
using ContactsApplication.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);





// Configure CORS
builder.Services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));

// Configure DB Context
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<ContactContext>(options => options.UseSqlServer(connectionString));

// Configure Swagger
builder.Services.AddSwaggerGen(c => c.SwaggerDoc("v1", new() { Title = "ContactsApplication", Version = "v1" }));

// Add other services
builder.Services.AddControllers();
builder.Services.AddHttpContextAccessor();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IContactRepository, ContactRepository>();
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddLogging();
// Load User Secrets in Development
if (builder.Environment.IsDevelopment())
{
    builder.Configuration.AddUserSecrets<Program>();
}

// Configure JwtSettings
builder.Services.Configure<JwtSettings>(builder.Configuration.GetSection("JwtSettings"));
// Build the application
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days.
    app.UseHsts();
}
app.UseCors("AllowAll");
app.UseAuthorization();
app.MapControllers();

app.Run();
