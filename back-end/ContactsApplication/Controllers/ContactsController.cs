﻿using ContactsApplication.Model;
using ContactsApplication.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Threading.Tasks;

namespace ContactsApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly IContactRepository _contactRepository;
        private readonly IUserService _userService;
        private readonly ILogger<ContactsController> _logger;
        public ContactsController(IContactRepository contactRepository, IUserService userService , ILogger<ContactsController> logger)
        {
            _contactRepository = contactRepository;
            _userService = userService;
            _logger = logger;
        }

        // GET: api/Contacts
        [HttpGet]
        public async Task<IActionResult> GetContactsAsync()
        {
            try
            {
                var contacts = await _contactRepository.GetContactsAsync();
                return Ok(contacts);
            }
            catch (Exception ex)
            {
                // Log the exception, ex.ToString()
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // GET: api/Contacts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContactAsync(int id)
        {
            try
            {
                var contact = await _contactRepository.GetContactAsync(id);
                if (contact == null)
                {
                    return NotFound();
                }
                return Ok(contact);
            }
            catch (Exception ex)
            {
                // Log the exception, ex.ToString()
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database");
            }
        }

        // PUT: api/Contacts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContactAsync(int id, Contact contact)
        {
            contact.ModifiedDate = DateTime.UtcNow;
            contact.ModifiedBy = _userService.GetCurrentUserId().ToString();
            var contactRepo = await _contactRepository.GetContactAsync(id);
            if (contactRepo == null)
            {
                return NotFound();
            }

            // Check if a contact with the same first name and last name already exists
            if (await _contactRepository.ContactExistsAsync(contact.FirstName, contact.LastName, id))
            {
                // Return a conflict status code (409) if a contact with the same first name and last name exists
                return Conflict("A contact with the same first name and last name already exists.");
            }

            contact.FirstName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.FirstName.ToLower());
            contact.LastName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.LastName.ToLower());
            contact.BillingAddress = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.BillingAddress.ToLower());
            contact.PhysicalAddress = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.PhysicalAddress.ToLower());

            await _contactRepository.UpdateContactAsync(contactRepo);
            await _contactRepository.SaveChangesAsync();
            return NoContent();
        }

        // POST: api/Contacts
        [HttpPost]
        public async Task<IActionResult> PostContactAsync([FromBody, Required] Contact contact)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode(StatusCodes.Status400BadRequest);
                }

                var newContact = await _contactRepository.AddContactAsync(contact);

                return StatusCode(StatusCodes.Status201Created, newContact);
            }
            catch (ArgumentException ex)
            {
                // Log the exception, ex.ToString()
                return StatusCode(StatusCodes.Status400BadRequest, $"Error creating new contact record: {ex.Message}");
            }
            catch (Exception ex)
            {
                // Log the exception, ex.ToString()
                _logger.LogError(ex, "Error creating new contact record"); // Assuming _logger is an ILogger instance
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error creating new contact record: {ex.Message}");
            }
        }
        // DELETE: api/Contacts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContactAsync(int id)
        {
            try
            {
                var contact = await _contactRepository.GetContactAsync(id);
                if (contact == null)
                {
                    return NotFound();
                }
                await _contactRepository.DeleteContactAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
    
                // Log the exception, ex.ToString()
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }
    }
}
